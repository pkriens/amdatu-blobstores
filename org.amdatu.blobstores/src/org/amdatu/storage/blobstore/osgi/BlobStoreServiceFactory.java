/*
 * Copyright (c) 2010, 2011 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore.osgi;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.amdatu.storage.blobstore.impl.BlobStoreServiceImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Service factory that configures BlobStoreService instances. A new instance is created for each provider.
 */
public class BlobStoreServiceFactory implements ManagedServiceFactory {
    private static final String ID_KEY = "id";
    private static final String PROVIDER_KEY = "provider";
    public final static String PID = "org.amdatu.storage.servicefactory";
    private static final String IDENTITY_KEY = "identity";
    private static final String SECRET_KEY = "secret";
    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_log;
    private final Map<String, Component> m_components = new HashMap<String, Component>();


    public String getName() {
            return "org.amdatu.storage.blobstorefactory";
    }

    public synchronized void updated(String pid, Dictionary properties)
            throws ConfigurationException {

        if (m_components.containsKey(pid)) {
            m_log.log(LogService.LOG_DEBUG, "pid already registered for provider '" + properties.get(PROVIDER_KEY) + "', skipping registration");
            return;
        }

        Properties serviceProperties = new Properties();

        String id = (String) properties.get(ID_KEY);
        String provider = (String) properties.get(PROVIDER_KEY);
        String identity = (String) properties.get(IDENTITY_KEY);
        String secret = (String) properties.get(SECRET_KEY);

        if (id == null) {
            m_log.log(LogService.LOG_ERROR, "Error while registering new BlobStoreService: id property not set");
            return;
        }

        if (provider == null) {
            m_log.log(LogService.LOG_ERROR, "Error while registering new BlobStoreService: provider property not set");
            return;
        }

        serviceProperties.setProperty(PROVIDER_KEY, provider);
        serviceProperties.setProperty(ID_KEY, id);

        if(identity != null) {
            serviceProperties.setProperty(IDENTITY_KEY, identity);
        }

        serviceProperties.setProperty(Constants.SERVICE_PID, pid);

        m_log.log(LogService.LOG_INFO, "Registering BlobStoreService for provider '" + serviceProperties.getProperty(PROVIDER_KEY) + "' with id " + id);

        BlobStoreServiceImpl blobStoreService = new BlobStoreServiceImpl(provider, identity, secret);

        Component component = m_dependencyManager
                .createComponent()
                .setInterface(BlobStoreService.class.getName(), serviceProperties)
                .setImplementation(blobStoreService)
                .add(m_dependencyManager.createServiceDependency().setService(LogService.class).setRequired(true));

        m_dependencyManager.add(component);
        m_components.put(pid, component);
    }

    public synchronized void deleted(String pid) {
        m_log.log(LogService.LOG_INFO, "Unregistering service with pid: '" + pid + "'");
        m_dependencyManager.remove(m_components.remove(pid));
    }

}
