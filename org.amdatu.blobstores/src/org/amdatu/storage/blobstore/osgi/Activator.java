/*
 * Copyright (c) 2010, 2011 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore.osgi;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Registers a ManagedServiceFactory that can be configured for different storage providers.
 */
public class Activator extends DependencyActivatorBase{
    @Override
    public void init(BundleContext bundleContext, DependencyManager dependencyManager) throws Exception {
    	Properties props = new Properties();
    	props.put(Constants.SERVICE_PID, BlobStoreServiceFactory.PID);

    	dependencyManager.add(createComponent()
    	  .setInterface(ManagedServiceFactory.class.getName(), props)
    	  .setImplementation(BlobStoreServiceFactory.class)
    	  .add(dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false)));
    }

    @Override
    public void destroy(BundleContext bundleContext, DependencyManager dependencyManager) throws Exception {
    }
}
