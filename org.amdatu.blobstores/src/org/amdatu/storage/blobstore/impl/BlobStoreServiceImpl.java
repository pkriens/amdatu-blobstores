/*
 * Copyright (c) 2010, 2011 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore.impl;

import java.util.Dictionary;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.log.LogService;

/**
 * Creates a BlobStoreContext for the m_provider, m_identity and secret configured by Configuration Admin.
 * Multiple instance of this service can coexist for using multiple blob store providers. 
 * Each service instance has a property "m_provider" that is one of {@link BlobStoreService#Providers}.
 * 
 */
public class BlobStoreServiceImpl implements BlobStoreService {
	private volatile LogService m_log;

	private String m_provider;
	private String m_identity;
	private String m_secretkey;

    public BlobStoreServiceImpl(String provider, String identity, String secretkey) {
        this.m_provider = provider;
        this.m_identity = identity;
        this.m_secretkey = secretkey;
    }

    public BlobStoreContext getContext() {
		m_log.log(LogService.LOG_DEBUG, "Creating context for m_provider '" + m_provider + "'" + " with m_identity '" + m_identity + "'");
		
		
		
		Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		BlobStoreContext context = ContextBuilder.newBuilder(m_provider)
				.credentials(m_identity, m_secretkey)
				.buildView(BlobStoreContext.class);
		return context;
		
	}

	public synchronized void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {		
		if (properties != null) {
			m_provider = (String) properties.get("m_provider");
			if (m_provider == null) {
				throw new ConfigurationException("m_provider",
						"Missing mandatory m_provider configuration");
			}

			m_identity = (String) properties.get("m_identity");
			if (m_identity == null) {
				throw new ConfigurationException("m_identity",
						"Missing mandatory m_identity configuration");
			}

			m_secretkey = (String) properties.get("secret");
			if (m_secretkey == null) {
				throw new ConfigurationException("secret",
						"Missing mandatory secret key configuration");
			}
		} 	
	}
}
