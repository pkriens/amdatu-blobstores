/*
 * Copyright (c) 2010, 2011 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore;

import java.util.Properties;

import org.jclouds.blobstore.BlobStoreContext;

/**
 * 
 * The BlobStoreService has the sole purpose of exposing the jclouds BlobStoreContext API. 
 * Using this service is more flexible than direct usage however because it can be configured using Configuration Admin. 
 */
public interface BlobStoreService {
	/**
	 * Retrieves the jclouds BlobStoreContext API. THe caller is responsible for closing the context.
	 */
	BlobStoreContext getContext();
	
	enum Providers{
		TRANSIENT("transient"),
		FILESYSTEM("filesystem"),
		EUCALYPTUS_PARTNERCLOUD_S3("eucalyptus-partnercloud-s3"),
		SYNAPTIC_STORAGE("synaptic-storage"),
		AZUREBLOB("azureblob"),
		CLOUDONESTORAGE("cloudonestorage"),
		CLOUDFILES_US("cloudfiles-us"),
		CLOUDFILES_UK("cloudfiles-uk"),
		NINEFOLD_STORAGE("ninefold-storage"),
		AWS_S3("aws-s3"),
		GOOGLESTORAGE("googlestorage"),
		SCALEUP_STORAGE("scaleup-storage"),
		HOSTEUROPE_STORAGE("hosteurope-storage"),
		TISCALI_STORAGE("tiscali-storage");
		
		private String id;
		
		Providers(String id) {
			this.id = id;
		}
		
		@Override
		public String toString() {
			return id;
		}
		
		public Properties getAsServiceProperties() {
			Properties props = new Properties();
			props.put("provider", id);
			return props;
		}
		
		public String getAsServiceFilter() {
			StringBuilder sb = new StringBuilder("(&(provider=");
			sb.append(id).append("))");
			return sb.toString();
		}
	}
}
