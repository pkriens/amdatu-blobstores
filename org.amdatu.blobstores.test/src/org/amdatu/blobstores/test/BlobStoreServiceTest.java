package org.amdatu.blobstores.test;

import java.util.Properties;

import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.PageSet;
import org.jclouds.blobstore.domain.StorageMetadata;

public class BlobStoreServiceTest extends BaseOSGiServiceTest<BlobStoreService> {
	private static final String CONTAINER_NAME = "jcloudstests";

	public BlobStoreServiceTest() {
		super(BlobStoreService.class);
	}
	
	@Override
	public void setUp() throws Exception {
		Properties props = new Properties();
		props.put("id", "test");
		props.setProperty("provider", BlobStoreService.Providers.TRANSIENT.toString());
		props.setProperty("identity", "user");
		props.setProperty("secret", "password");
		configureFactory("org.amdatu.storage.servicefactory", props);

		super.setUp();
	}

	public void testBlobStoreService() {
		
		BlobStoreContext context = instance.getContext();
		BlobStore blobStore = context.getBlobStore();
		blobStore.createContainerInLocation(null, CONTAINER_NAME);
		
		PageSet<? extends StorageMetadata> list = blobStore.list(CONTAINER_NAME);
		
		assertEquals(0, list.size());

		blobStore.putBlob(CONTAINER_NAME, blobStore.blobBuilder("myblob").payload("somecontent").build());
		list = blobStore.list();
		
		assertEquals(1, list.size());
		context.close();
		
		
	}
}
